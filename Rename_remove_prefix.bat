@echo off

echo.============================
echo FileType Ex:
echo 1:*.*
echo 2:*.txt
echo 3:otherType.txt = other*y*.txt
echo.============================
SET /p "FileType=FileType:"

echo.
echo.============================
echo.Ex: C:\Test
echo.============================
SET /p "SrcPath=SrcPath:"

echo.
echo.============================
echo Ex:///* (removing 3 chars)
echo.============================
SET /p "rename_syntax=rename_syntax:"

echo.============================
REM forfiles /P "%SrcPath%" /M *.* /S  /C "cmd /c echo @path" > wrk.lst
forfiles /P "%SrcPath%" /M %FileType% /S  /C "cmd /c echo @path" > wrk.lst
for /F "usebackq tokens=*" %%I in (".\wrk.lst") do  (
	rename %%I %rename_syntax%
)

echo v FINISHED & pause>nul

REM ref.
REM https://stackoverflow.com/questions/10202540/rename-or-remove-prefix-multiple-files-to-each-ones-number
REM https://stackoverflow.com/questions/11719347/using-forfiles-with-multiple-file-types-for-search-mask
REM https://www.biostars.org/p/323148/